# horusec-kotlin helpers 2020-10-24 by @k33g | on gitlab.com 
FROM horuszup/horusec-kotlin:latest

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN apk --update add --no-cache nodejs npm

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]
