#!/usr/bin/env node

console.log("👮‍♀️ horusec-kotlin analysis")

const fs = require("fs")
const { exec } = require("child_process")

/* ===== tools ===== */
function toTitleCase(str) {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

let cmdArgs = process.argv.slice(2);
let directory = cmdArgs[0]

let scannerName = "Horusec-Kotlin"
let scannerId = "horusec-kotlin"
let scannerSrc = "https://bots.garden"
let scannerVendor = "Bots.Garden"
let scannerVersion = "0.0.1"

let start = new Date()

// ========== Helpers ==========
let get_dependency = () => { 
  return { 
    package: {} 
  } 
}   
      
let get_scanner = () => {
  return {
    id: scannerId, name: scannerName
  }
}
// ========== End of Helpers ==========

let cmd = `horusec-kotlin run -p ${directory} -o horusec-kotlin.json`

exec(cmd, (error, stdout, stderr) => {

  if (error) {
    console.log(`error: ${error}`)
    /* --- generate empty gl-code-quality-report.json --- */
    fs.writeFileSync("./gl-sast-report.json", "[]")
    return
  }

  if (stderr) {
    console.log(`stderr: ${stderr}`)
    /* --- generate empty gl-code-quality-report.json --- */
    fs.writeFileSync("./gl-sast-report.json", "[]")
    return
  }

  try {
    let content = fs.readFileSync("horusec-kotlin.json")
    //console.log(JSON.parse(content))
    let horusecResults = JSON.parse(content)

    /* --- generate gl-sast-report.json --- */
    let vulnerabilities = horusecResults.map(item => {
      return {
        category: "sast",
        name: `🔴: ${item.Name}`,
        message: `📝: ${item.Name}`,
        description: `🖐️: ${item.Description}`,
        cve: item.ID,
        severity: toTitleCase(item.Severity),
        confidence: toTitleCase(item.Confidence),
        scanner: get_scanner(),
        location: {
          file: item.SourceLocation.Filename.replace(process.env.CI_PROJECT_DIR ,""), 
          start_line: item.SourceLocation.Line,
          end_line: item.SourceLocation.Line,
          class: item.CodeSample,
          method: "horusec-kotlin",
          dependency: get_dependency()
        },
        identifiers: [
          {
            type: "horusec_kotlin_rule_id",
            name: `${item.ID}`,
            value: item.CodeSample
          }
        ]        
      } // return
    })

    let end = new Date() - start

    let sastReport = {
      version: "3.0",
      vulnerabilities: vulnerabilities,
      remediations: [],
      scan: {
        scanner: {
          id: scannerId,
          name: scannerName,
          url: scannerSrc,
          vendor: {
            name: scannerVendor
          },
          version: scannerVersion
        },
        type: "sast",
        start_time: start,
        end_time: end,
        status: "success" // TODO try catch
      }
    }
    console.log(sastReport)

    fs.writeFileSync("./gl-sast-report.json", JSON.stringify(sastReport, null, 2))

  } catch(error) {
    console.log(`😡 catch error: ${error}`)
    /* --- generate empty gl-code-quality-report.json --- */
    fs.writeFileSync("./gl-sast-report.json", "[]")
  }
  console.log(`stdout: ${stdout}`)
})


/*
[
  {
    "ID": "ef15bf33-7099-4112-9570-a5a337e292df",
    "Name": "No Log Sensitive Information",
    "Severity": "INFO",
    "CodeSample": "println(\"🤖😀 Funktion is starting 🎉 , you can call it soon\")",
    "Confidence": "LOW",
    "Description": "The App logs information. Sensitive information should never be logged. For more information checkout the CWE-532 (https://cwe.mitre.org/data/definitions/532.html) advisory.",
    "SourceLocation": {
      "Filename": "/builds/tanuki-workshops/kube-demos/vulnerabilitiy-analyzers/horusec-kotlin/sample/src/funktionCheck.kt",
      "Line": 15,
      "Column": 8
    }
  }
]
*/
